const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const crypto = require("crypto");
const moment = require("moment");
const morgan = require("morgan");
const axios = require("axios");
const mysql = require("./mysql.js");
const mysqlLib = require("mysql");

app.use(morgan("combined"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/station/list", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      connection.query("SELECT * FROM station", function(
        error,
        results,
        fields
      ) {
        if (error) return res.status(500).send(error);
        return res.status(200).send(results);
      });
    }
  });
});

app.get("/station/data/lastDay/:id", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      console.log(req.params.id);
      const currentDate = new Date();
      console.log(moment(new Date()).format("YYYY-MM-DD"));
      connection.query(
        "SELECT *\
        FROM data\
        WHERE id_station IN (SELECT id FROM station WHERE station.uuid = ?) AND time >= ?",
        [
          req.params.id,
          moment(currentDate)
            .subtract(1, "days")
            .format("YYYY-MM-DD")
        ],
        function(error, results, fields) {
          if (error) return res.status(500).send(error);
          const filteredValues = [...new Set(results.map(obj => obj.type))];
          return res.status(200).send({ data: results, type: filteredValues });
        }
      );
    }
  });
});

app.get("/station/data/:id", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      console.log(req.params.id);
      connection.query(
        "SELECT *\
        FROM data\
        WHERE id_station IN (SELECT id FROM station WHERE station.uuid = ?) AND id IN (\
            SELECT MAX(id)\
            FROM data\
            GROUP BY type\
        );",
        [req.params.id],
        function(error, results, fields) {
          if (error) return res.status(500).send(error);
          return res.status(200).send(
            results.map(item => {
              return {
                type: item.type,
                data: item.data,
                date: item.time
              };
            })
          );
        }
      );
    }
  });
});

app.get("/station/data", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      connection.query("SELECT * FROM data", function(error, results, fields) {
        if (error) return res.status(500).send(error);
        return res.status(200).send(results);
      });
    }
  });
});

app.post("/station/create", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      console.log("Connected to MySQL - Thread " + connection.threadId);
      crypto.randomBytes(48, function(err, buffer) {
        var token = buffer.toString("hex");
        var code = Math.floor(Math.random() * 90000) + 10000;
        var uuid = req.body.uuid;
        var post = { token, uuid, code };
        var query = connection.query(
          "INSERT INTO station SET ?",
          post,
          function(error, results, fields) {
            if (error) {
              return res
                .status(500)
                .send("An Error occured when creating the station");
            }
            connection.end();
            return res.status(200).send({ code });
          }
        );
      });
    }
  });
});

app.post("/station/subscribe", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      console.log("Connected to MySQL - Thread " + connection.threadId);
      connection.query(
        "SELECT * FROM station WHERE code = ?",
        [req.body.code],
        function(error, results, fields) {
          if (error) console.log(error);
          if (results.length < 1) {
            connection.end();
            return res.status(403).send("Your code is incorrect");
          }
          const token = results[0].token;
          const uuid = results[0].uuid;
          const id = results[0].id;
          axios
            .post("http://api:5000/station/activate", { uuid })
            .then(response => {
              if (response.status === 200) {
                connection.query(
                  `UPDATE station SET code = "0" WHERE station.id = ?`,
                  [id],
                  function(error, results, fields) {
                    if (error) console.log(error);
                    connection.end();
                    return res.status(200).send(token);
                  }
                );
              } else {
                return res
                  .status(500)
                  .send("An error occured on the server: no response from api");
              }
            })
            .catch(err => {
              console.log(err);
              return res.status(500).send("An error occured on the server");
            });
        }
      );
    }
  });
});

app.post("/station/", function(req, res) {
  const connection = mysql.connect();

  connection.connect(function(err) {
    if (err) {
      console.log("Could not connect to MySQL " + err.stack);
      return res.status(500).send("An error occured on the server");
    } else {
      console.log("Connected to MySQL - Thread " + connection.threadId);
      connection.query(
        "SELECT * FROM station WHERE token = ?",
        [req.body.token],
        function(error, results, fields) {
          if (error) console.log(error);
          if (results.length < 1) {
            connection.end();
            return res.status(403).send("Your are not authenticated");
          }
          const id = results[0].id;
          const formatedData = req.body.data
            .replace(/"/g, "")
            .split(/(?:\\)(?:[tn\\]*)/);
          let query = "INSERT INTO data (id_station, type, data, time) VALUES ";
          formatedData.map(item => {
            if (item.length > 0) {
              const string = item.split(";");
              query = `${query} ('${mysqlLib.escape(
                results[0].id
              )}', ${mysqlLib.escape(string[0])}, ${mysqlLib.escape(
                string[1]
              )}, ${mysqlLib.escape(
                moment(string[2], "ddd MMM DD H:m:s gggg").format("Y-M-D H:m:s")
              )}),`;
            }
          });
          query = query.substring(0, query.length - 1);
          query = `${query};`;
          connection.query(query, function(error, results, fields) {
            if (error) {
              console.log("ERROR INSERTING DATA", error);
              return res.status(500).send("An error occured on the server");
            }
            crypto.randomBytes(48, function(err, buffer) {
              var token = buffer.toString("hex");
              connection.query(
                "UPDATE station SET token = ? WHERE id = ?",
                [token, id],
                function(error, results, fields) {
                  if (error) {
                    console.log("ERROR UPDATING TOKEN", error);
                    return res
                      .status(500)
                      .send("An error occured on the server");
                  }
                  connection.end();
                  return res.status(200).send(token);
                }
              );
            });
          });
        }
      );
    }
  });
});

const server = app.listen(9000, "0.0.0.0");
