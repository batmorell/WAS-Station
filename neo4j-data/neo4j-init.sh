#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# do not run init script at each container strat but only at the first start
if [ ! -f /tmp/neo4j-import-done.flag ]; then
    echo "Import database..."
    /var/lib/neo4j/bin/neo4j-admin load --from=/data/export.dump --database=graph.db --force=true
    echo "Import succeded."
    touch /tmp/neo4j-import-done.flag
else
    echo "The import has already been made."
fi