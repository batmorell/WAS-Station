const requireLogin = require('../middlewares/requireLogin');
const requireTeacher = require('../middlewares/requireTeacher');
const activitiesController = require('../controllers/activitiesController');

module.exports = (app) => {
  app.post('/activity', requireTeacher, activitiesController.createNewActivity);

  app.post('/activity/:id/check', requireLogin, activitiesController.checkActivity);

  app.get(
    '/activity/student/:themeId/:studentId',
    requireTeacher,
    activitiesController.getEvalActivityForStudent,
  );

  app.put('/activity/:id', requireTeacher, activitiesController.setActivityData);

  app.get('/activity/:id', requireLogin, activitiesController.getActivity);

  app.post('/addActivityIntoTheme', requireTeacher, activitiesController.addIntoTheme);

  app.delete('/activity/:id', requireTeacher, activitiesController.deleteActivity);
};
