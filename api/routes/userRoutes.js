const requireTeacher = require('../middlewares/requireTeacher');
const userController = require('../controllers/usersController');

module.exports = (app) => {
  app.post('/users', userController.createNewUser);

  app.post('/users/createStudent', requireTeacher, userController.createNewStudent);

  app.put('/users', requireTeacher, userController.updateUser);

  app.delete('/users/:username', requireTeacher, userController.deleteUser);
};
