const requireLogin = require('../middlewares/requireLogin');
const requireTeacher = require('../middlewares/requireTeacher');
const themesController = require('../controllers/themesController');

module.exports = (app) => {
  app.post('/themes', requireTeacher, themesController.createNewTheme);

  app.post('/addThemeToClasse', requireTeacher, themesController.addThemeToClasse);

  app.post('/removeThemeToClasse', requireTeacher, themesController.removeThemeToClasse);

  app.get('/themes', requireLogin, themesController.getMyThemes);

  app.get('/themes/student/:id', requireTeacher, themesController.getThemeForStudent);

  app.get('/theme/classe/:classeId', requireTeacher, themesController.getThemeForClasse);

  app.get('/theme/:themeUuid/activities', requireLogin, themesController.getThemeActivities);

  app.put('/theme/title', requireTeacher, themesController.updateThemeTitle);

  app.get('/theme/:id', requireLogin, themesController.getATheme);
};
