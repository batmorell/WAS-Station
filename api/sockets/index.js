const io = require('socket.io')();
const userController = require('../controllers/usersController');

io.on('connection', (client) => {
  client.on('startActivity', (data) => {
    userController.startActivity(data);
    client.on('disconnect', () => {
      userController.stopActivity(data.userId);
    });
  });
  client.on('stopActivity', (uuid) => {
    userController.stopActivity(uuid);
  });
  client.on('getActiveStudent', (data) => {
    setInterval(() => {
      userController.getActiveStudent(data.userId, results =>
        client.emit('activeStudentList', results));
    }, data.freq);
  });
  client.on('end', () => {
    client.disconnect(0);
  });
});

io.listen(7000);
