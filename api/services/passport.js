const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Model = require('../models');
const omit = require('object.omit');

passport.serializeUser((user, done) => {
  done(null, user.uuid);
});

passport.deserializeUser((uuid, done) => {
  Model.User.getByUuid(uuid, (err, user) => {
    user = JSON.parse(JSON.stringify(user[0]));
    const deserializedUser = omit(user.Person.properties, 'password');
    done(null, deserializedUser);
  });
});

passport.use(new LocalStrategy((username, password, done) => {
  Model.User.getByUsername(username, (err, user) => {
    if (err) {
      return done(err);
    }
    if (!user || user.length < 1) {
      return done({ message: 'Pseudo incorrecte' }, false);
    }
    user = JSON.parse(JSON.stringify(user[0].Person.properties));
    Model.User.validPassword(password, user.password, (err, res) => {
      if (err) return done(err);
      if (!res) return done({ message: 'Mot de passe incorrecte' }, false);
      return done(null, omit(user, 'password'));
    });
  });
}));
