const express = require('express');
const session = require('express-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
require('./services/passport');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: `${process.env.SESSION_SECRET}` }));
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan('combined'));

require('./sockets');

require('./routes/authRoutes')(app);
require('./routes/userRoutes')(app);
require('./routes/classeRoutes')(app);
require('./routes/themeRoutes')(app);
require('./routes/activityRoutes')(app);
require('./routes/stationRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT);
