const Model = require('../models/index.js');
const omit = require('object.omit');

const ClassesController = {
  createNewClasse(req, res) {
    const { name } = req.body;
    Model.Classe.create({ name }, req.user, (err) => {
      if (err) return console.error(err);
      res.status(200).send('classe created');
    });
  },
  addUserIntoClasse(req, res) {
    const { uuid, name } = req.body;
    Model.Classe.addUserIntoClasse(uuid, name, (err, result) => {
      if (err) return console.error(err);
      res.status(200).send(result);
    });
  },
  getMyClasses(req, res) {
    Model.Classe.getUserClasses(req.user, (err, result) => {
      if (err) return console.error(err);
      if (!result || result.length < 1) return res.status(200).send([]);
      res.status(200).send(result.map(item => item.classes.properties));
    });
  },
  getAClasse(req, res) {
    Model.Classe.getAClasse(req.params.id, (err, result) => {
      if (!result || result.length < 1) return res.status(204).send('Classe not found');
      if (err) return console.error(err);
      res.status(200).send({
        ...result[0].classe.properties,
        total: result[0].total,
        students: result[0].students.map(item => ({
          ...omit(item.student.properties, ['password', 'role']),
          number: item.number,
          total: item.total,
          eval: item.eval,
          best: item.best,
        })),
      });
    });
  },
};

module.exports = ClassesController;
