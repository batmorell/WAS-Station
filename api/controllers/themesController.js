const Model = require('../models/index.js');
const omit = require('object.omit');

const ThemesController = {
  createNewTheme(req, res) {
    if (!req.user) return res.status(403).send('Not authenticate');
    const { name } = req.body;
    return Model.Theme.create({ name }, req.user, (err) => {
      if (err) return console.error(err);
      return res.status(200).send('theme created');
    });
  },
  addThemeToClasse(req, res) {
    const { themeUuid, classeUuid } = req.body;
    Model.Theme.addThemeToClasse(themeUuid, classeUuid, (err) => {
      if (err) return console.error(err);
      res.status(200).send('theme added to classe');
    });
  },
  removeThemeToClasse(req, res) {
    const { themeUuid, classeUuid } = req.body;
    Model.Theme.removeThemeToClasse(themeUuid, classeUuid, (err) => {
      if (err) return console.error(err);
      res.status(200).send('theme remove to classe');
    });
  },
  getATheme(req, res) {
    const { id } = req.params;
    if (!req.user) return res.status(403).send('Not authenticate');
    if (req.user.role === 'teacher') {
      Model.Theme.getThemeTeacher(id, (err, theme) => {
        if (err) return console.error(err);
        if (!theme || theme.length < 1) return res.status(204).send('Theme not found');
        listOfClasses = theme[0].listOfClasses.map(item => item.properties);
        theme = theme[0].n.properties;
        Model.Theme.getThemeActivities(id, (err, activities) => {
          if (!activities || activities.length < 1) {
            return res.status(200).send({
              ...theme,
              listOfClasses,
              activities: [],
            });
          }
          activities = activities[0].a.map(item => omit(item.properties, ['data', 'soluce']));
          return res.status(200).send({
            ...theme,
            listOfClasses,
            activities,
          });
        });
      });
    } else {
      Model.Theme.getTheme(id, req.user.uuid, (err, theme) => {
        if (err) return console.error(err);
        if (!theme || theme.length < 1) return res.status(204).send('Theme not found');
        const { score } = theme[0];
        theme = theme[0].n.properties;
        Model.Theme.getThemeActivitiesStudent(id, req.user, (err, activities) => {
          if (!activities || activities.length < 1) {
            return res.status(200).send({
              ...theme,
              activities: [],
            });
          }
          activities = activities[0].listOfActivities.map((item, index) => ({
            ...omit(item.properties, ['data', 'soluce']),
            done: activities[0].isDone[index],
          }));
          return res.status(200).send({
            ...theme,
            activities,
            score,
          });
        });
      });
    }
  },
  getThemeForClasse(req, res) {
    const { classeId } = req.params;
    Model.Theme.getThemeForClasse(classeId, (err, result) => {
      if (!result || result.length < 1) return res.status(200).send([]);
      res.status(200).send(result.map(element => ({
        ...element.t.properties,
        total: element.total,
        number: element.number,
      })));
    });
  },
  getThemeActivities(req, res) {
    const { themeUuid } = req.params;
    if (!req.user) return res.status(403).send('Not authenticate');
    Model.Theme.getThemeActivity(themeUuid, req.user, (err, results) =>
      res.status(200).send(results));
  },
  getThemeForStudent(req, res) {
    Model.Theme.getMyClasseThemes(req.params.id, (err, results) => {
      if (!results || results.length < 1) return res.status(200).send([]);
      return res.status(200).send(results.map(item => ({
        ...item.theme.theme.properties,
        totalNumber: item.theme.totalNumber,
        numberOfCompleted: item.theme.numberOfCompleted,
        score: item.theme.score,
      })));
    });
  },
  getMyThemes(req, res) {
    if (!req.user) return res.status(403).send('Not authenticate');
    const { uuid } = req.user;
    if (req.user.role === 'teacher') {
      Model.Theme.getTeacherThemes(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        if (err) return console.error(err);
        results = results.map(item => ({
          ...item.n.properties,
          totalNumber: item.totalNumber,
        }));
        return res.status(200).send(results);
      });
    } else {
      Model.Theme.getMyClasseThemes(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        return res.status(200).send(results.map(item => ({
          ...item.theme.theme.properties,
          totalNumber: item.theme.totalNumber,
          numberOfCompleted: item.theme.numberOfCompleted,
          score: item.theme.score,
        })));
      });
    }
  },
  updateThemeTitle(req, res) {
    if (!req.user) return res.status(403).send('Not authenticate');
    const { themeUuid, title } = req.body;
    Model.Theme.updateThemeTitle(themeUuid, title, () => res.status(200).send('Title updated'));
  },
};

module.exports = ThemesController;
