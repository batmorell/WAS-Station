const Model = require('../models/index.js');
const omit = require('object.omit');

const UsersController = {
  createNewUser(req, res) {
    const { username, password, role } = req.body;
    Model.User.create({ username, password, role }, (err) => {
      if (err) return console.error(err);
      res.status(200).send('user created');
    });
  },
  getActiveStudent(userId, callback) {
    Model.User.getActiveStudent(userId, (err, results) => {
      if (err) return console.error(err);
      const formatedResults = results.map(item => ({
        person: item.person.person,
        theme: item.theme.theme,
        listOfActivities: item.listOfActivities.map(element =>
          omit(element.properties, ['data', 'soluce'])),
        isDone: item.isDone,
        current: item.current,
      }));
      callback(formatedResults);
    });
  },
  startActivity(data) {
    Model.User.startActivity(data, (err) => {
      if (err) return console.error(err);
    });
  },
  stopActivity(userId) {
    Model.User.stopActivity(userId, (err) => {
      if (err) return console.error(err);
    });
  },
  createNewStudent(req, res) {
    const { username, password, classeUuid } = req.body;
    Model.User.createStudent({ username, password, role: 'student' }, classeUuid, (err) => {
      if (err) return console.error(err);
      res.status(200).send('user created');
    });
  },
  updateUser(req, res) {
    const { username } = req.body;
    Model.User.update(username, req.body, (err) => {
      if (err) return console.error(err);
      res.status(200).send('user updated');
    });
  },
  deleteUser(req, res) {
    Model.User.delete(req.params.username, (err) => {
      if (err) return res.status(500).send('An error occured trying to delete user');
      res.status(200).send(`User ${req.params.username} deleted`);
    });
  },
};

module.exports = UsersController;
