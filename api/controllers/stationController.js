const Model = require('../models/index.js');
const axios = require('axios');

const ActivitiesController = {
  createNewStation(req, res) {
    const { name, place } = req.body;
    const userId = req.user.uuid;
    Model.Station.create({ name, place }, userId, (err, uuid) => {
      if (err) return console.error(err);
      axios
        .post('http://dataserver:9000/station/create', { uuid })
        .then((response) => {
          if (response.status === 200) {
            return res.status(200).send(response.data);
          }
          return res.status(500).send('Error creating station');
        })
        .catch((error) => {
          console.log(error);
        });
    });
  },
  activateStation(req, res) {
    const { uuid } = req.body;
    Model.Station.activate({ uuid }, (err, results) => {
      if (err) {
        res.status(500).send('Internal error');
        return console.error(err);
      }
      if (results.length < 1) {
        return res.status(500).send('Cannot find this station');
      }
      res.status(200).send('station activated');
    });
  },
  getStationData(req, res) {
    const { id } = req.params;
    const { uuid } = req.user;
    if (req.user.role === 'teacher') {
      Model.Station.getMyStationIDTeacher(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        axios
          .get(`http://dataserver:9000/station/data/${results[0].id}`)
          .then((response) => {
            if (response.status === 200) {
              return res.status(200).send(response.data);
            }
            return res.status(500).send('Error getting data');
          })
          .catch((error) => {
            console.log(error);
          });
      });
    } else {
      Model.Station.getMyStationID(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        axios
          .get(`http://dataserver:9000/station/data/${results[0].id}`)
          .then((response) => {
            if (response.status === 200) {
              return res.status(200).send(response.data);
            }
            return res.status(500).send('Error getting data');
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }
  },
  getStationLastData(req, res) {
    const { id } = req.params;
    const { uuid } = req.user;
    if (req.user.role === 'teacher') {
      Model.Station.getMyStationIDTeacher(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(204).send([]);
        axios
          .get(`http://dataserver:9000/station/data/lastDay/${results[0].id}`)
          .then((response) => {
            if (response.status === 200) {
              return res.status(200).send(response.data.type.map(element => ({
                type: element,
                data: response.data.data
                  .filter(item => item.type === element)
                  .map(a => ({ data: a.data, date: a.time })),
              })));
            }
            return res.status(500).send('Error getting data');
          })
          .catch((error) => {
            console.log(error);
          });
      });
    } else {
      Model.Station.getMyStationID(uuid, (err, results) => {
        if (!results || results.length < 1) return res.status(204).send([]);
        axios
          .get(`http://dataserver:9000/station/data/lastDay/${results[0].id}`)
          .then((response) => {
            if (response.status === 200) {
              return res.status(200).send(response.data.type.map(element => ({
                type: element,
                data: response.data.data
                  .filter(item => item.type === element)
                  .map(a => ({ data: a.data, date: a.time })),
              })));
            }
            return res.status(500).send('Error getting data');
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }
  },
  getMyStations(req, res) {
    const { id } = req.params;
    if (req.user.role === 'teacher') {
      Model.Station.getMyStationsTeacher(id, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        res.status(200).send(results.map(element => element.st.properties));
      });
    } else {
      Model.Station.getMyStations(id, (err, results) => {
        if (!results || results.length < 1) return res.status(200).send([]);
        res.status(200).send(results.map(element => element.st.properties));
      });
    }
  },
};

module.exports = ActivitiesController;
