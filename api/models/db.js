const neo4j = require('neo4j');

const db = new neo4j.GraphDatabase(`http://neo4j:${process.env.NEO4J_PASS}@neo4j:7474`);

module.exports = db;
