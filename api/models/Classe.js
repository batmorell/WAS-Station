const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(classe, user, callback) {
      const uuid = uuidv4();
      query(`CREATE (classe:Classe { name: '${classe.name}', uuid: '${uuid}' })`, () => {
        query(
          `MATCH (a:Person),(b:Classe) WHERE a.uuid = '${user.uuid}' AND b.uuid = '${uuid}'
            CREATE (a)-[r:HAS]->(b) RETURN b.uuid`,
          callback,
        );
      });
    },
    getUserClasses(user, callback) {
      query(
        `MATCH (:Person { uuid: '${user.uuid}'})-[:HAS]->(classes)
          RETURN classes`,
        callback,
      );
    },
    getAClasse(id, callback) {
      query(
        `MATCH (c:Classe { uuid: '${id}'})
          OPTIONAL MATCH (c)-[:HAS]->(:Theme)<-[:ISINTO]-(a:Activity)
          OPTIONAL MATCH (c)<-[:ISIN]-(p:Person)
          OPTIONAL MATCH (a)<-[d:DONE]-(p)
          OPTIONAL MATCH (a { type: 'eval' })<-[e:DONE]-(p)
          WITH c, count(distinct a) AS total, count(d) as number, p, sum(toInt(e.correct)) as score, sum(toInt(e.total)) as best
          RETURN c AS classe, collect({ total: total, student: p, number: number, eval: score, best: best }) AS students;`,
        (err, result) => callback(null, result),
      );
    },
    addUserIntoClasse(classeId, username, callback) {
      query(
        `MATCH (a:Person),(b:Classe) WHERE a.name = '${username}' AND b.uuid = '${classeId}'
            CREATE (a)-[r:ISIN]->(b) RETURN r`,
        callback,
      );
    },
  };
};
