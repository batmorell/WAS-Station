const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(theme, user, callback) {
      const uuid = uuidv4();
      query(`CREATE (theme:Theme { name: '${theme.name}', uuid: '${uuid}' })`, () => {
        query(
          `MATCH (a:Theme { uuid: '${uuid}'}), (b:Person { uuid: '${
            user.uuid
          }'}) CREATE (a)<-[r:HASCREATED]-(b) return r`,
          callback,
        );
      });
    },
    addThemeToClasse(themeUuid, classeUuid, callback) {
      query(
        `MATCH (a:Theme),(b:Classe) WHERE a.uuid = '${themeUuid}' AND b.uuid = '${classeUuid}'
                    CREATE (a)<-[r:HAS]-(b) RETURN r`,
        callback,
      );
    },
    removeThemeToClasse(themeUuid, classeUuid, callback) {
      query(
        `MATCH (a:Theme { uuid: '${themeUuid}' })<-[r:HAS]-(b:Classe { uuid: '${classeUuid}'})
                    DELETE r`,
        callback,
      );
    },
    getTheme(themeUuid, userId, callback) {
      query(
        `MATCH (n:Theme { uuid: '${themeUuid}'}) OPTIONAL MATCH (:Person { uuid: '${userId}' })-[d:DONE]->(:Activity { type: 'eval' })-[:ISINTO]-(n) RETURN n, { correct: d.correct, total: d.total } as score;`,
        callback,
      );
    },
    getThemeTeacher(themeUuid, callback) {
      query(
        `MATCH (n:Theme { uuid: '${themeUuid}'}) OPTIONAL MATCH (n)<-[:HAS]-(c:Classe) RETURN n, collect(c) AS listOfClasses`,
        callback,
      );
    },
    getThemeActivities(themeUuid, callback) {
      query(
        `MATCH (a:Activity)-[:ISINTO]->(:Theme { uuid: '${themeUuid}'})
          WHERE NOT ()-[:HASNEXT]->(a:Activity)
          OPTIONAL MATCH (a)-[:HASNEXT*]->(b:Activity)
          RETURN a + collect(b) AS a`,
        callback,
      );
    },
    getThemeActivitiesStudent(themeUuid, user, callback) {
      query(
        `MATCH (a:Activity)-[:ISINTO]->(:Theme { uuid: '${themeUuid}'})
          WHERE NOT ()-[:HASNEXT]->(a:Activity)
          OPTIONAL MATCH (a)-[:HASNEXT*]->(b:Activity)
          RETURN a + collect(b) AS listOfActivities, exists((a)<-[:DONE]-(:Person { uuid: '${
  user.uuid
}' })) + collect(exists((b)<-[:DONE]-(:Person { uuid: '${user.uuid}' }))) AS isDone`,
        callback,
      );
    },
    getThemeForClasse(classeId, callback) {
      query(
        `MATCH (t:Theme)<-[:HAS]-(c:Classe { uuid: '${classeId}'})<-[:ISIN]-(p:Person)
        MATCH (t)<-[:ISINTO]-(a:Activity)
        OPTIONAL MATCH (a)<-[d:DONE]-(:Person)-[:ISIN]-(c)
        return t, count(distinct a) * count(distinct p) AS total, count(distinct d) AS number`,
        callback,
      );
    },
    getTeacherThemes(userUuid, callback) {
      query(
        `MATCH (n:Theme)
          WHERE (:Person {uuid: '${userUuid}'})-[:HAS]->(:Classe)-[:HAS]->(n)
          OR (:Person {uuid: '${userUuid}'})-[:HASCREATED]->(n)
          OPTIONAL MATCH (n)<-[:ISINTO]-(a:Activity)
          RETURN n, count(a) AS totalNumber`,
        callback,
      );
    },
    getMyClasseThemes(userUuid, callback) {
      query(
        `MATCH (p:Person { uuid: '${userUuid}'})-[:ISIN]->(:Classe)-[:HAS]->(t:Theme)
          OPTIONAL MATCH (t)<-[:ISINTO]-(a:Activity)
          OPTIONAL MATCH (t)<-[:ISINTO]-(b:Activity)<-[:DONE]-(p)
          OPTIONAL MATCH (t)<-[:ISINTO]-(:Activity { type: 'eval' })<-[d:DONE]-(p)
          RETURN { theme: t, totalNumber: count(distinct a), numberOfCompleted: count(distinct b), score: { correct: d.correct, total: d.total } } AS theme`,
        callback,
      );
    },
    updateThemeTitle(themeUuid, title, callback) {
      query(`MATCH (t:Theme { uuid: '${themeUuid}' }) SET t.name = '${title}'`, callback);
    },
  };
};
