const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(station, userId, callback) {
      const uuid = uuidv4();
      query(
        `CREATE (st:Station { name: '${station.name}', uuid: '${uuid}', place: '${station.place}' })
        WITH st
        MATCH (c:Classe)<-[:HAS]-(p:Person { uuid: '${userId}' })
        WITH c, p, st
        CREATE (st)<-[:HAS]-(c)`,
        (err, results) => callback(err, uuid),
      );
    },
    activate(station, callback) {
      query(
        `MATCH (st:Station
        { uuid: '${station.uuid}' }) SET st.activated = true RETURN st`,
        callback,
      );
    },
    getMyStationID(uuid, callback) {
      query(
        `MATCH (st:Station)<-[:HAS]-(:Classe)<-[:ISIN]-(p:Person { uuid: '${uuid}' }) RETURN st.uuid as id`,
        callback,
      );
    },
    getMyStationIDTeacher(uuid, callback) {
      query(
        `MATCH (st:Station)<-[:HAS]-(:Classe)<-[:HAS]-(p:Person { uuid: '${uuid}' }) RETURN st.uuid as id`,
        callback,
      );
    },
    getMyStations(userId, callback) {
      query(
        `MATCH (st:Station)<-[:HAS]-(:Classe)<-[:ISIN]-(p:Person { uuid: '${userId}' }) RETURN st`,
        callback,
      );
    },
    getMyStationsTeacher(userId, callback) {
      query(
        `MATCH (st:Station)<-[:HAS]-(:Classe)<-[:HAS]-(p:Person { uuid: '${userId}' }) RETURN distinct st`,
        callback,
      );
    },
  };
};
