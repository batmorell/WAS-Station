const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(activity, callback) {
      const uuid = uuidv4();
      query(
        `CREATE (ac:Activity { name: '${activity.name}', uuid: '${uuid}', type: '${
          activity.type
        }' }) WITH ac 
          MATCH (t:Theme { uuid: '${activity.idTheme}'}) CREATE (ac)-[:ISINTO]->(t) WITH ac,t 
          MATCH (previousAc:Activity)-[:ISINTO]->(t) WHERE NOT (previousAc)-[:HASNEXT]->(:Activity) AND ac.uuid <> previousAc.uuid
          FOREACH(o IN CASE WHEN previousAc IS NOT NULL THEN [previousAc] ELSE [] END | CREATE (previousAc)-[:HASNEXT]->(ac))`,
        (err, results) => callback(err, uuid),
      );
    },
    updateDataActivity(activityUuid, data, soluce, callback) {
      query(
        `MATCH (ac:Activity {uuid: '${activityUuid}'}) SET ac.data = '${data}', ac.soluce = '${soluce}' RETURN ac`,
        callback,
      );
    },
    addIntoTheme(themeUuid, activityUuid, callback) {
      query(
        `MATCH (a:Theme),(b:Activity) WHERE a.uuid = '${themeUuid}' AND b.uuid = '${activityUuid}'
                    CREATE (a)<-[r:ISINTO]-(b) RETURN r`,
        callback,
      );
    },
    findLastActivity(themeUuid, callback) {
      query(
        `MATCH (:Theme { uuid: '${themeUuid}' })<-[:ISINTO]-(n:Activity) WHERE NOT (n:Activity)-[:HASNEXT]->(:Activity) RETURN n`,
        callback,
      );
    },
    getNextActivityUuid(activityUuid, callback) {
      query(
        `MATCH (:Activity {uuid: '${activityUuid}'})-[:HASNEXT]->(n:Activity) RETURN n.uuid`,
        callback,
      );
    },
    getActivitySoluce(activityUuid, callback) {
      query(`MATCH (ac:Activity {uuid: '${activityUuid}'}) RETURN ac.soluce`, callback);
    },
    getActivity(activityUuid, userId, callback) {
      query(
        `MATCH (n:Activity {uuid: '${activityUuid}'}) OPTIONAL MATCH (n)-[d:DONE]-(:Person { uuid: '${userId}' }) RETURN n, d`,
        callback,
      );
    },
    getEvalActivityForStudent(themeId, studentId, callback) {
      query(
        `MATCH (n:Activity {type: 'eval'})-[:ISINTO]->(:Theme { uuid: '${themeId}'})
        OPTIONAL MATCH (n)-[d:DONE]-(:Person { uuid: '${studentId}' }) RETURN n, d`,
        callback,
      );
    },
    addATry(activityUuid, user, correct, total, callback) {
      query(
        `MATCH (a:Activity {uuid: '${activityUuid}'}), (b:Person {uuid: '${
          user.uuid
        }'}) CREATE (a)<-[:TRY { correct: '${correct}', total: '${total}'}]-(b)`,
        callback,
      );
    },
    completeActivity(activityUuid, user, correct, total, callback) {
      query(
        `MATCH (a:Activity {uuid: '${activityUuid}'}), (b:Person {uuid: '${
          user.uuid
        }'}) CREATE (a)<-[:DONE { correct: '${correct}', total: '${total}'}]-(b)`,
        callback,
      );
    },
    completeEval(activityUuid, user, correct, total, results, soluces, callback) {
      query(
        `MATCH (a:Activity {uuid: '${activityUuid}'}), (b:Person {uuid: '${
          user.uuid
        }'}) CREATE (a)<-[:DONE { correct: '${correct}', total: '${total}', results: '${JSON.stringify(results)}', soluces: '${JSON.stringify(soluces)}'}]-(b)`,
        callback,
      );
    },
    delete(activityUuid, callback) {
      query(
        `MATCH(ac:Activity{uuid:'${activityUuid}'})
        OPTIONAL MATCH(acf:Activity)<-[:HASNEXT]-(ac)
        OPTIONAL MATCH (ac)<-[:HASNEXT]-(acp:Activity) WITH acf, ac, acp
        DETACH DELETE ac
        FOREACH(o IN CASE WHEN acp IS NOT NULL AND acf IS NOT NULL THEN [acp] ELSE [] END | CREATE (acf)<-[:HASNEXT]-(acp))`,
        callback,
      );
    },
  };
};
