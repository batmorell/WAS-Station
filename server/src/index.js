import "babel-polyfill";
import express from "express";
import proxy from "express-http-proxy";
import renderer from "./helpers/renderer";

const app = express();

// Define here a proxy to redirect api call
app.use("/api", proxy("http://api:5000"));

app.use(express.static("public"));

// All others routes are redirected to react router
app.get("*", (req, res) => {
  res.send(renderer());
});

app.listen(3000);
