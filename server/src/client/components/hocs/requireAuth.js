import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from '../../components/Loading';
import { Redirect } from 'react-router-dom';
import { fetchCurrentUser } from '../../actions';

export default ChildComponent => {
  class RequireAuth extends Component {
    componentWillMount() {
      this.props.fetchCurrentUser()
    }
    render() {
      switch (this.props.auth) {
        case false:
          return <Redirect to="/login" />;
        case null:
          return <Loading />;
        default:
          return <ChildComponent {...this.props} />;
      }
    }
  }

  function mapStateToProps({ auth }) {
    return { auth };
  }

  return connect(mapStateToProps, { fetchCurrentUser })(RequireAuth);
};
