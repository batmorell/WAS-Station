import React, { Component } from "react";
import * as d3 from "d3";

class RadialProgressChart extends Component {
  componentDidMount() {
    const svg = d3.select(`#i${this.props.id}`);

    const parent = d3.select(svg.node().parentNode);
    const width = parseInt(svg.style("width"));

    const outerRadius = width * 0.5 / 2;
    const innerRadius = outerRadius - width * 0.1;

    const height = outerRadius;
    const aspect = 2;

    svg
      .attr("viewBox", `0 0 ${width} ${outerRadius}`)
      .attr("preserveAspectRatio", "xMinYMid")
      .call(() => this.resize(parent, svg, aspect));

    d3.select(window).on(`resize.${this.props.id}`, () => this.resize(parent, svg, aspect));

    const g = svg.append("g").attr("transform", `translate(${width / 2}, ${this.props.arc ? outerRadius : height / 2})`);

    let angularValue = this.props.arc ? this.props.currentValue / this.props.maxValue * Math.PI : this.props.currentValue / this.props.maxValue * 2 * Math.PI;
    angularValue = angularValue || 0;

    const arc = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(5)
      .startAngle(() => (this.props.arc ? 3 / 2 * Math.PI : 0))
      .endAngle(d => this.scale(d));

    const container = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(5)
      .startAngle(() => (this.props.arc ? 3 / 2 * Math.PI : 0))
      .endAngle(() => (this.props.arc ? 5 / 2 * Math.PI : 2 * Math.PI));

    g
      .append("path")
      .attr("class", `${this.props.id}-bg`)
      .style("fill", "#eeeeee")
      .attr("d", container);

    const foreground = g
      .append("path")
      .datum({ endAngle: 0 })
      .attr("class", `${this.props.id}-fg`)
      .style("fill", this.props.color)
      .attr("d", arc(5 / 2 * Math.PI));

    g
      .append("text")
      .text(`${this.props.maxValue > 0 ? Math.trunc(this.props.currentValue / this.props.maxValue * 100) : 0}%`)
      .attr("fill", "#545454")
      .attr("font-size", "20px")
      .attr("transform", "translate(-15, -3)");

    const customElastic = d3.easeElasticOut.period(0.9);

    function arcTween(newAngle) {
      return d => {
        const interpolate = d3.interpolate(d.endAngle, newAngle);
        return t => {
          d.endAngle = interpolate(t);
          return arc(d);
        };
      };
    }

    foreground
      .transition()
      .ease(customElastic)
      .duration(1500)
      .attrTween("d", arcTween(angularValue));
  }

  scale = d => {
    if (typeof d === "number") return d;
    return this.props.arc ? d.endAngle + 3 / 2 * Math.PI : d.endAngle;
  };

  resize = (parent, svg, aspect) => {
    const targetWidth = parseInt(parent.style("width"));

    svg.attr("width", targetWidth);
    svg.attr("height", Math.round(targetWidth / aspect));
  };

  render() {
    return (
      <div style={{ marginBottom: 20, backgroundColor: "#fafafa" }}>
        <svg width="100%" id={`i${this.props.id}`} />
      </div>
    );
  }
}

RadialProgressChart.defaultProps = {
  arc: true,
  color: "#76bf5a"
};

export default RadialProgressChart;
