import React, { Component } from "react";
import { Form, Radio, Checkbox } from "antd";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;

class QuizzResult extends Component {
  constructor(props) {
    super(props);

    let results = [];
    let soluce = [];

    try {
      results = JSON.parse(props.result.results);
      soluce = JSON.parse(props.result.soluces);
    } catch (err) {
      results = props.result.results;
      soluce = props.result.soluces;
    }

    const stateConstructor = props.data.map((element, index) => {
      const value = results[index].response;
      if (results[index].answer) {
        return {
          [`q${element.id}`]: {
            value,
            validateStatus: "",
            errorMsg: "Bonne réponse"
          }
        };
      }
      const answer = soluce[index].reponse.map(e => (e.answer ? e.id : null)).filter(i => i !== null);
      const textAnswer = element.reponse.filter(a => answer.indexOf(a.id) !== -1).map(e => e.text);
      return {
        [`q${element.id}`]: {
          value,
          validateStatus: "error",
          errorMsg: (
            <div>
              <p> Mauvaise Réponse </p>
              <p>La bonne réponse était {textAnswer.join(", ")}</p>
            </div>
          )
        }
      };
    });

    function shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    }

    const quizz = props.data.map(question => ({
      ...question,
      reponse: shuffle(question.reponse)
    }));

    this.state = {
      result: stateConstructor,
      quizz
    };
  }

  displayAnswer = item => {
    const radioStyle = {
      display: "block",
      height: "30px",
      lineHeight: "30px"
    };
    const { type } = item;

    return item.reponse.map(
      element =>
        type === "unique" ? (
          <Radio key={element.id} style={radioStyle} value={element.id}>
            {element.text}
          </Radio>
        ) : (
          <Checkbox key={element.id} value={element.id} defaultChecked>
            {element.text}
          </Checkbox>
        )
    );
  };

  displayQuizz = () =>
    this.state.quizz.map((item, index) => (
      <FormItem
        key={item.id}
        label={item.question}
        validateStatus={this.state.result[index][`${Object.keys(this.state.result[index])[0]}`].validateStatus}
        help={this.state.result[index][`${Object.keys(this.state.result[index])[0]}`].errorMsg}
      >
        {item.type === "unique" ? (
          <RadioGroup disabled value={this.state.result[index][`${Object.keys(this.state.result[index])[0]}`].value}>
            {this.displayAnswer(item)}
          </RadioGroup>
        ) : (
          <CheckboxGroup disabled value={this.state.result[index][`${Object.keys(this.state.result[index])[0]}`].value}>
            {this.displayAnswer(item)}
          </CheckboxGroup>
        )}
      </FormItem>
    ));

  render() {
    return <Form>{this.displayQuizz()}</Form>;
  }
}

export default QuizzResult;
