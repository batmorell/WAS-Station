import React from "react";
import { Row, Col, Button, Progress } from "antd";

const ThemeCard = props => (
  <div style={{ marginBottom: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
    <Row type="flex" justify="center">
      <Col>
        <h1 style={{ textAlign: "center" }}>
          {props.theme.name} {props.theme.score && props.theme.score.correct && `- ${props.theme.score.correct}/${props.theme.score.total}`}
        </h1>
      </Col>
    </Row>
    {!props.teacher ? (
      <div>
        <Row type="flex" justify="center" style={{ marginBottom: 20 }}>
          <Progress type="circle" percent={props.theme.totalNumber > 0 ? parseInt(props.theme.numberOfCompleted / props.theme.totalNumber * 100, 10) : 0} />
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <p>{`${props.theme.totalNumber} activité(s)`}</p>
          </Col>
        </Row>
      </div>
    ) : (
      <div style={{ marginBottom: 10 }}>
        <Row type="flex" justify="center">
          <Col>
            <h1 style={{ marginBottom: 0, fontSize: 50 }}>{`${props.theme.totalNumber}`}</h1>
          </Col>
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <p>activité(s)</p>
          </Col>
        </Row>
      </div>
    )}
    <Row type="flex" justify="center">
      <Col>
        <Row type="flex" justify="center">
          <Button disabled={!(props.theme.score && props.theme.score.correct)} type="primary" onClick={() => props.navigate(props.theme.uuid)}>
            Consulter
          </Button>
        </Row>
      </Col>
    </Row>
  </div>
);

export default ThemeCard;
