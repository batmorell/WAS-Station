import React from "react";
import { Avatar, Row, Col } from "antd";

const UserInfoCard = props => (
  <div style={{ marginBottom: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
    <Row type="flex" justify="center">
      <Col>
        <Avatar src={props.image} size="large" style={{ height: 100, width: 100, borderRadius: 50 }} />
      </Col>
    </Row>
    <h1 style={{ textAlign: "center", marginTop: 20, marginBottom: 20 }}>{props.name}</h1>
  </div>
);

export default UserInfoCard;
