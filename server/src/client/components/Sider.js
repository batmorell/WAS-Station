import React from "react";
import { Link } from "react-router-dom";
import { Layout, Menu, Icon } from "antd";

const { Sider } = Layout;
const { SubMenu } = Menu;

const renderTeacherSider = () => (
  <Sider style={{ overflow: "auto", height: "100vh", position: "fixed", left: 0 }}>
    <div className="logo" />
    <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
      <Menu.Item key="0" style={{ height: 100 }}>
        <img src="/img/logo-small.png" style={{ width: "100%" }} alt="Logo" />
      </Menu.Item>
      <Menu.Item key="1">
        <Link to="/">
          <Icon type="home" />Home
        </Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link to="/classes">
          <Icon type="book" />Mes classes
        </Link>
      </Menu.Item>
      <Menu.Item key="3">
        <Link to="/themes">
          <Icon type="folder" />Thèmes
        </Link>
      </Menu.Item>
      <Menu.Item key="5">
        <Link to="/clientStation/teacher">
          <Icon type="calculator" />Mes stations
        </Link>
      </Menu.Item>
      <Menu.Item key="6">
        <Link to="/activeStudent">
          <Icon type="eye-o" />Étudiants actifs
        </Link>
      </Menu.Item>
      <Menu.Item key="7" style={{ position: "absolute", bottom: 0 }}>
        <a href="/api/logout">
          <Icon type="logout" />Logout
        </a>
      </Menu.Item>
    </Menu>
  </Sider>
);

const renderStudentSider = () => (
  <Sider style={{ overflow: "auto", height: "100vh", position: "fixed", left: 0 }}>
    <div className="logo" />
    <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
      <Menu.Item key="0" style={{ height: 100 }}>
        <img src="/img/logo-small.png" style={{ width: "100%" }} alt="Logo" />
      </Menu.Item>
      <Menu.Item key="1">
        <Link to="/">
          <Icon type="home" />Home
        </Link>
      </Menu.Item>
      {/* <SubMenu
        key="sub1"
        title={
          <span>
            <Icon type="user" />
            <span>Mon compte</span>
          </span>
        }
      >
        <Menu.Item key="2">Avancement</Menu.Item>
        <Menu.Item key="3">Mes Badges</Menu.Item>
      </SubMenu> */}
      <Menu.Item key="4">
        <Link to="/themes">
          <Icon type="folder" />Thèmes
        </Link>
      </Menu.Item>
      <Menu.Item key="5">
        <Link to="/clientStation">
          <Icon type="calculator" />Ma station
        </Link>
      </Menu.Item>
      {/* <Menu.Item key="6">
        <Link to="/stationMap">
          <Icon type="environment" />Carte des stations
        </Link>
    </Menu.Item> */}
      <Menu.Item key="7" style={{ position: "absolute", bottom: 0 }}>
        <a href="/api/logout">
          <Icon type="logout" />Logout
        </a>
      </Menu.Item>
    </Menu>
  </Sider>
);

const SiderComp = props => (props.role === "teacher" ? renderTeacherSider() : renderStudentSider());

export default SiderComp;
