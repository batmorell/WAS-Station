import React, { Component } from "react";
import { Form, Radio, Checkbox } from "antd";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;

class Quizz extends Component {
  constructor(props) {
    super(props);

    const stateConstructor = props.data.map(element => ({
      [`q${element.id}`]: {
        value: null,
        validateStatus: "",
        errorMsg: ""
      }
    }));

    function shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    }

    const quizz = props.data.map(question => ({
      ...question,
      reponse: shuffle(question.reponse)
    }));

    this.state = {
      result: stateConstructor,
      quizz
    };
  }

  componentDidUpdate = () => {
    if (this.props.waitingValue === true) {
      let responseMissing = false;

      let values = {};

      this.state.result.map(item => {
        responseMissing = item[Object.keys(item)[0]].value === null;
        if (!responseMissing) {
          values = {
            ...values,
            [Object.keys(item)[0]]: item[Object.keys(item)[0]].value
          };
        }
      });

      if (!responseMissing) {
        this.props.sendAnswer(value => {
          value.map(item => {
            const resultUpdate = this.state.result;

            resultUpdate.map((element, index) => {
              if (item.id === Object.keys(element)[0]) {
                resultUpdate[index][item.id].validateStatus = item.answer ? "success" : "error";
                resultUpdate[index][item.id].errorMsg = item.answer ? "Bonne(s) Réponse(s)" : "Mauvaise(s) Réponse(s)";
              }
            });

            this.setState({
              result: resultUpdate
            });
          });
        }, values);
      } else {
        this.props.sendAnswer(null, undefined);

        const resultUpdate = this.state.result;

        resultUpdate.map(element => {
          if (element[Object.keys(element)[0]].value === null) {
            element[Object.keys(element)[0]].validateStatus = "error";
            element[Object.keys(element)[0]].errorMsg = "Répondez à la question !";
          }
        });

        this.setState({
          result: resultUpdate
        });
      }
    }
  };

  getIndexCollectionById = (collection, id) => collection.reduce((acc, v, ind) => (Object.keys(v)[0] === id ? ind : acc), -1);

  handleChange = (e, name) => {
    const resultUpdate = this.state.result;
    resultUpdate.map((element, index) => {
      if (name === Object.keys(element)[0]) {
        resultUpdate[index][name] = {
          value: e instanceof Array ? e : e.target.value,
          validateStatus: "",
          errorMsg: ""
        };
      }
    });

    this.setState({
      result: resultUpdate
    });
  };

  displayAnswer = item => {
    const radioStyle = {
      display: "block",
      height: "30px",
      lineHeight: "30px"
    };
    const { type } = item;

    return item.reponse.map(
      element =>
        type === "unique" ? (
          <Radio key={element.id} style={radioStyle} value={element.id}>
            {element.text}
          </Radio>
        ) : (
          <Checkbox key={element.id} value={element.id}>
            {element.text}
          </Checkbox>
        )
    );
  };

  displayQuizz = () =>
    this.state.quizz.map(item => (
      <FormItem
        key={item.id}
        label={item.question}
        validateStatus={this.state.result[this.getIndexCollectionById(this.state.result, `q${item.id}`)][`q${item.id}`].validateStatus}
        help={this.state.result[this.getIndexCollectionById(this.state.result, `q${item.id}`)][`q${item.id}`].errorMsg}
      >
        {item.type === "unique" ? (
          <RadioGroup
            onChange={e => {
              this.handleChange(e, `q${item.id}`);
            }}
          >
            {this.displayAnswer(item)}
          </RadioGroup>
        ) : (
          <CheckboxGroup
            onChange={e => {
              this.handleChange(e, `q${item.id}`);
            }}
          >
            {this.displayAnswer(item)}
          </CheckboxGroup>
        )}
      </FormItem>
    ));

  render() {
    return <Form>{this.displayQuizz()}</Form>;
  }
}

export default Quizz;
