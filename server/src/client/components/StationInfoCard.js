import React, { Component } from "react";
import { Row, Col } from "antd";

const StationInfoCard = props => (
  <div style={{ marginBottom: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
    <Row type="flex" justify="center">
      <h1>Données Station</h1>
      <hr style={{ width: "80%", margin: "auto" }} />
      {props.data.filter(item => item.type === "T").length > 0 &&
        props.pos === 1 && (
          <Col span={24}>
            <Row type="flex" justify="center">
              <h2>Température</h2>
            </Row>
            <Row type="flex" justify="center">
              <h2>{props.data.filter(item => item.type === "T")[0].data}°C</h2>
            </Row>
          </Col>
        )}
      {props.data.filter(item => item.type === "P").length > 0 &&
        props.pos === 1 && (
          <Col span={24}>
            <Row type="flex" justify="center">
              <h2>Pression</h2>
            </Row>
            <Row type="flex" justify="center">
              <h2>{props.data.filter(item => item.type === "P")[0].data} hPa</h2>
            </Row>
          </Col>
        )}
      {props.data.filter(item => item.type === "H").length > 0 &&
        props.pos === 2 && (
          <Col span={24}>
            <Row type="flex" justify="center">
              <h2>Humidité</h2>
            </Row>
            <Row type="flex" justify="center">
              <h2>{props.data.filter(item => item.type === "H")[0].data} %</h2>
            </Row>
          </Col>
        )}
      {props.data.filter(item => item.type === "V").length > 0 &&
        props.pos === 2 && (
          <Col span={24}>
            <Row type="flex" justify="center">
              <h2>Vent</h2>
            </Row>
            <Row type="flex" justify="center">
              <h2>{props.data.filter(item => item.type === "V")[0].data} km/h</h2>
            </Row>
          </Col>
        )}
    </Row>
  </div>
);

export default StationInfoCard;
