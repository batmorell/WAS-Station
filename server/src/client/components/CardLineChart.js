import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as d3 from "d3";

class CardLineChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      margin: { top: 20, right: 20, bottom: 30, left: 50 }
    };
  }

  componentDidMount() {
    const svg = d3.select(`#${this.props.id}`);

    const width = parseInt(svg.style("width")) - this.state.margin.left - this.state.margin.right;
    const height = parseInt(svg.style("height")) - this.state.margin.top - this.state.margin.bottom;

    const parseTime = d3.timeParse("%Y-%m-%dT%H:%M:%S.000Z");

    const x = d3.scaleTime().rangeRound([0, width]);
    const y = d3.scaleLinear().rangeRound([height, 0]);

    x.domain(d3.extent(this.props.data, d => parseTime(d.date)));
    y.domain(d3.extent(this.props.data, d => d.data));

    // create a line function that can convert data[] into x and y points
    const line = d3
      .line()
      // assign the X function to plot our line as we wish
      .x(d => x(parseTime(d.date)))
      .y(d => y(d.data));

    // Add an SVG element with the desired dimensions and margin.
    const graph = svg
      .append("g")
      .attr("id", `${this.props.id}-g`)
      .attr("transform", `translate(${this.state.margin.left},${this.state.margin.top})`)
      .attr("width", width)
      .attr("height", height);

    const xAxis = graph
      .append("g")
      .attr("transform", `translate(0,${height})`)
      .call(d3.axisBottom(x).ticks(4));

    xAxis.select(".domain").remove();

    graph
      .append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(y))
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text(this.props.labelY);

    // Add the line by appending an svg:path element with the data line we created above
    // do this AFTER the axes above so that the line is above the tick-lines
    graph
      .append("svg:path")
      .attr("class", "line")
      .attr("fill", "none")
      .attr("stroke", "#76bf5a")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 3)
      .attr("d", line(this.props.data));

    // ////

    this.setState({
      data: this.props.data
    });

    // this.resize();
  }

  getTitle = () => {
    switch (this.props.type) {
      case "V":
        return "Vent";
      case "H":
        return "Humidité";
      case "U":
        return "UV";
      case "P":
        return "Pression";
      case "O":
        return "Poussière";
      case "T":
        return "Température";
      case "L":
        return "Pluie";
      default:
        return "";
    }
  };

  render() {
    return (
      <div style={{ marginBottom: 20, backgroundColor: "#fafafa" }}>
        <h1 style={{ textAlign: "center" }}>{this.getTitle()}</h1>
        <svg width="100%" height="400" id={this.props.id} />
      </div>
    );
  }
}

export default CardLineChart;
