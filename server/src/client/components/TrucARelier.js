import React from "react";
import * as d3 from "d3";
import omit from "object.omit";
import { Row } from "antd";

class TrucARelier extends React.Component {
  componentDidMount() {
    const svg = d3.select("svg").call(
      d3
        .drag()
        .container(function assign() {
          return this;
        })
        .subject(() => {
          const p = [d3.event.x, d3.event.y];
          return [p, p];
        })
        .on("start", this.dragstarted)
    );
    const parent = d3.select(svg.node().parentNode);

    function shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    }

    const width = 640;
    const height = 420;
    const aspect = width / height;

    svg
      .attr("viewBox", `0 0 ${width} ${height}`)
      .attr("preserveAspectRatio", "xMidYMin")
      .call(() => this.resize(parent, svg, aspect));

    d3.select(window).on(`resize.${this.props.id}`, () => this.resize(parent, svg, aspect));

    const startZones = [];
    const endZones = [];

    const elementHeight = height / this.props.data.startZones.length > 200 ? 200 : height / this.props.data.startZones.length;

    shuffle(this.props.data.startZones).map((item, index) => {
      svg
        .append("g")
        .append("image")
        .attr("transform", `translate(0,${index * elementHeight})`)
        .attr("xlink:href", item.img)
        .attr("width", elementHeight)
        .attr("height", elementHeight);

      startZones.push({
        coord: [elementHeight + 20, index * elementHeight + elementHeight / 2],
        occupedId: null,
        id: item.id
      });

      svg
        .append("circle")
        .attr("cy", index * elementHeight + elementHeight / 2)
        .attr("cx", elementHeight + 20)
        .attr("r", 5)
        .attr("stroke", "black");
    });

    shuffle(this.props.data.endZones).map((item, index) => {
      svg
        .append("g")
        .append("image")
        .attr("transform", `translate(${width - elementHeight},${index * elementHeight})`)
        .attr("xlink:href", item.img)
        .attr("width", elementHeight)
        .attr("height", elementHeight);

      endZones.push({
        coord: [width - elementHeight - 30, index * elementHeight + elementHeight / 2],
        occupedId: null,
        id: item.id
      });

      svg
        .append("circle")
        .attr("cy", index * elementHeight + elementHeight / 2)
        .attr("cx", width - elementHeight - 30)
        .attr("r", 5)
        .attr("stroke", "black");
    });

    this.setState({ startZones, endZones, response: {} });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.responses.length > 1) {
      nextProps.responses.map(item => {
        if (!item.result) {
          const falseElements = this.state.startZones.filter(element => element.id === item.idStartZone);
          falseElements.map(element => {
            d3.select(`#i${element.occupedId}`).attr("style", "stroke:rgb(255,0,0)");
          });
        }
      });
      this.props.cleanResponse();
    }
  }

  resize = (parent, svg, aspect) => {
    let targetWidth = parseInt(parent.style("width"), 10);
    if (targetWidth > 600) {
      targetWidth = 600;
    }

    svg.attr("width", targetWidth);
    svg.attr("height", Math.round(targetWidth / aspect));
  };

  generateRandomId = () =>
    Math.random()
      .toString(36)
      .substr(2, 9);

  cleanEndIfUse = index => {
    if (this.state.endZones[index].occupedId) {
      d3.select(`#i${this.state.endZones[index].occupedId}`).remove();
    }
  };

  dragended = (active, id, indexClosestStart) => {
    const indexClosest = this.findClosestEndPoint(d3.event.x, d3.event.y);
    if (indexClosest !== -1) {
      this.cleanEndIfUse(indexClosest);
      this.setState(
        {
          endZones: this.state.endZones.map((item, index) => {
            if (index === indexClosest) {
              return {
                ...this.state.endZones[index],
                occupedId: id
              };
            }
            return this.state.endZones[index];
          }),
          response: {
            ...omit(this.state.response, [], val => val !== this.state.endZones[indexClosest].id),
            [this.state.startZones[indexClosestStart].id]: this.state.endZones[indexClosest].id
          }
        },
        () => {
          this.sendDataToParent();
        }
      );
      active.attr("x2", this.state.endZones[indexClosest].coord[0]).attr("y2", this.state.endZones[indexClosest].coord[1]);
    } else {
      d3.select(`#i${id}`).remove();
    }
  };

  dragendedInvert = (active, id, indexClosestEnd) => {
    const indexClosest = this.findClosestStartPoint(d3.event.x, d3.event.y);
    if (indexClosest !== -1) {
      this.cleanStartIfUse(indexClosest);
      this.setState(
        {
          startZones: this.state.startZones.map((item, index) => {
            if (index === indexClosest) {
              return {
                ...this.state.startZones[index],
                occupedId: id
              };
            }
            return this.state.startZones[index];
          }),
          response: {
            ...omit(this.state.response, [], val => val !== this.state.endZones[indexClosestEnd].id),
            [this.state.startZones[indexClosest].id]: this.state.endZones[indexClosestEnd].id
          }
        },
        () => {
          this.sendDataToParent();
        }
      );

      active.attr("x2", this.state.startZones[indexClosest].coord[0]).attr("y2", this.state.startZones[indexClosest].coord[1]);
    } else {
      d3.select(`#i${id}`).remove();
    }
  };

  sendDataToParent = () => {
    this.props.validate(() => {}, this.state.response);
  };

  cleanStartIfUse = index => {
    if (this.state.startZones[index].occupedId) {
      d3.select(`#i${this.state.startZones[index].occupedId}`).remove();
    }
  };

  dragstarted = () => {
    let indexClosest = this.findClosestStartPoint(d3.event.x, d3.event.y);
    if (indexClosest !== -1) {
      this.cleanStartIfUse(indexClosest);
      const id = this.generateRandomId();
      const svg = d3.select("svg");
      const active = svg
        .append("line")
        .attr("id", `i${id}`)
        .attr("x1", this.state.startZones[indexClosest].coord[0])
        .attr("y1", this.state.startZones[indexClosest].coord[1])
        .attr("x2", d3.event.x)
        .attr("y2", d3.event.y);

      this.setState({
        startZones: this.state.startZones.map((item, index) => {
          if (index === indexClosest) {
            return {
              ...this.state.startZones[index],
              occupedId: id
            };
          }
          return this.state.startZones[index];
        })
      });

      d3.event.on("drag", () => {
        const x2 = d3.event.x;
        const y2 = d3.event.y;

        active.attr("x2", x2).attr("y2", y2);
      });

      d3.event.on("end", () => this.dragended(active, id, indexClosest));
    } else {
      indexClosest = this.findClosestEndPoint(d3.event.x, d3.event.y);
      if (indexClosest !== -1) {
        this.cleanEndIfUse(indexClosest);
        const id = this.generateRandomId();
        const svg = d3.select("svg");
        const active = svg
          .append("line")
          .attr("id", `i${id}`)
          .attr("x1", this.state.endZones[indexClosest].coord[0])
          .attr("y1", this.state.endZones[indexClosest].coord[1])
          .attr("x2", d3.event.x)
          .attr("y2", d3.event.y);

        this.setState({
          endZones: this.state.endZones.map((item, index) => {
            if (index === indexClosest) {
              return {
                ...this.state.endZones[index],
                occupedId: id
              };
            }
            return this.state.endZones[index];
          })
        });

        d3.event.on("drag", () => {
          const x2 = d3.event.x;
          const y2 = d3.event.y;

          active.attr("x2", x2).attr("y2", y2);
        });

        d3.event.on("end", () => this.dragendedInvert(active, id, indexClosest));
      }
    }
  };

  findClosestStartPoint = (x, y) => {
    let distance = Infinity;
    let closestIndex = -1;
    this.state.startZones.map((item, index) => {
      const currentDistance = this.distance(x, item.coord[0], y, item.coord[1]);
      if (currentDistance < distance && currentDistance < 40) {
        closestIndex = index;
        distance = currentDistance;
      }
    });
    return closestIndex;
  };

  findClosestEndPoint = (x, y) => {
    let distance = Infinity;
    let closestIndex = -1;
    this.state.endZones.map((item, index) => {
      const currentDistance = this.distance(x, item.coord[0], y, item.coord[1]);
      if (currentDistance < distance && currentDistance < 40) {
        closestIndex = index;
        distance = currentDistance;
      }
    });
    return closestIndex;
  };

  distance = (x1, x2, y1, y2) => Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);

  render() {
    return (
      <Row type="flex" justify="center">
        <svg width="100%" height="100%">
          <rect fill="#fff" width="100%" height="100%" />
        </svg>
      </Row>
    );
  }
}

export default TrucARelier;
