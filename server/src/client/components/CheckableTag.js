import React from 'react';
import { Tag } from 'antd';

const { CheckableTag } = Tag;

class MyTag extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.initialValue,
    };
  }

  handleChange = (checked) => {
    this.setState({ checked });
    this.props.handleChange(this.props.id, checked, this.props.children);
  };

  render() {
    return (
      <CheckableTag checked={this.state.checked} onChange={this.handleChange}>
        {this.props.children}
      </CheckableTag>
    );
  }
}

export default MyTag;
