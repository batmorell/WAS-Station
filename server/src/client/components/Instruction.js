import React, { Component } from "react";
import { Row, Col } from "antd";

class Instruction extends Component {
  renderElement = item => {
    switch (item.type) {
      case "image":
        return (
          <Row type="flex" justify="center">
            <Col span={16}>
              <img src={`${item.content}`} width="100%" style={{ marginBottom: `20px` }} />
            </Col>
          </Row>
        );
      case "text":
        return (
          <div
            style={{
              textAlign: `justify`,
              textJustify: `inter-word`,
              marginBottom: `20px`
            }}
          >
            {item.content}
          </div>
        );
      default:
        break;
    }
  };

  render() {
    return this.props.data.map(item => (
        <Row type="flex" justify="center">
          <Col span={14}>{this.renderElement(item)}</Col>
        </Row>
      ));
  }
}

export default Instruction;
