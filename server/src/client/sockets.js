import openSocket from "socket.io-client";

let socket = openSocket("https://bmorellab.tech");

function startActivity(userId, activityId) {
  socket.emit("startActivity", { userId, activityId });
}

function stopActivity(uuid) {
  socket.emit("stopActivity", uuid);
}
function fetchActiveStudent(uuid, cb) {
  if (socket.disconnected) {
    socket = openSocket("https://bmorellab.tech");
  }
  socket.on("activeStudentList", list => cb(null, list));
  socket.emit("getActiveStudent", { userId: uuid, freq: 5000 });
}

function closeConnection() {
  socket.emit("end");
}

export { startActivity, stopActivity, fetchActiveStudent, closeConnection };
