import React from "react";
import App from "./App";
import HomePage from "./pages/HomePage";
import ClassesPage from "./pages/ClassesPage";
import ClassePage from "./pages/ClassePage";
import ThemesListPage from "./pages/ThemesListPage";
import ThemePage from "./pages/ThemePage";
import ThemeTeacherPage from "./pages/ThemeTeacherPage";
import AddThemePage from "./pages/AddThemePage";
import EditThemePage from "./pages/EditThemePage";
import NotFoundPage from "./pages/NotFoundPage";
import AdminsListPage from "./pages/AdminsListPage";
import LoginPage from "./pages/LoginPage";
import AddClassePage from "./pages/AddClassePage";
import AddStudentPage from "./pages/AddStudentPage";
import ActivityPage from "./pages/ActivityPage";
import MyStationPage from "./pages/MyStationPage";
import MyStationTeacherPage from "./pages/MyStationTeacherPage";
import AddStationPage from "./pages/AddStationPage";
import ActiveStudentPage from "./pages/ActiveStudentPage";
import AddActivityPage from "./pages/AddActivityPage";
import EditActivityPage from "./pages/EditActivityPage";
import StudentInfoPage from "./pages/StudentInfoPage";

export default [
  {
    ...LoginPage,
    path: "/login",
    exact: true
  },
  {
    ...App,
    routes: [
      {
        ...HomePage,
        path: "/",
        exact: true
      },
      {
        ...ClassesPage,
        path: "/classes",
        exact: true
      },
      {
        ...ClassePage,
        path: "/classe/:id"
      },
      {
        ...AddClassePage,
        path: "/classes/add"
      },
      {
        ...StudentInfoPage,
        path: "/student/:id"
      },
      {
        ...AddStudentPage,
        path: "/classes/addStudent"
      },
      {
        ...AdminsListPage,
        path: "/admins"
      },
      {
        ...AddThemePage,
        path: "/themes/add"
      },
      {
        ...ThemesListPage,
        path: "/themes"
      },
      {
        ...EditThemePage,
        path: "/theme/edit/:id"
      },
      {
        ...ThemeTeacherPage,
        path: "/theme/:id/teacher"
      },
      {
        ...ThemePage,
        path: "/theme/:id"
      },
      {
        ...AddActivityPage,
        path: "/activity/add"
      },
      {
        ...EditActivityPage,
        path: "/activity/edit/:id"
      },
      {
        ...ActivityPage,
        path: "/activity/:id"
      },
      {
        ...AddStationPage,
        path: "/clientStation/add"
      },
      {
        ...MyStationTeacherPage,
        path: "/clientStation/teacher"
      },
      {
        ...MyStationPage,
        path: "/clientStation/"
      },
      {
        ...ActiveStudentPage,
        path: "/activeStudent/"
      },
      {
        ...NotFoundPage
      }
    ]
  }
];
