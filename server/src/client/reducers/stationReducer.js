import { CREATE_STATION, FETCH_DATA_STATION, GET_MY_STATIONS, FETCH_LAST_DATA_STATION } from "../actions";

const initialState = {
  code: "",
  dataStation: [],
  lastDataStation: [],
  list: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STATION:
      return {
        ...state,
        code: action.payload.data
      };
    case FETCH_DATA_STATION:
      return {
        ...state,
        dataStation: action.payload.data
      };
    case GET_MY_STATIONS:
      return {
        ...state,
        list: action.payload.data
      };
    case FETCH_LAST_DATA_STATION:
      return {
        ...state,
        lastDataStation: action.payload.data
      };
    default:
      return state;
  }
};
