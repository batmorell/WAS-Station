import { FETCH_MY_CLASSES, FETCH_A_CLASSE, FETCH_THEMES_CLASSE } from "../actions";

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_MY_CLASSES:
      return {
        ...state,
        list: action.payload.data
      };
    case FETCH_A_CLASSE:
      console.log(action.payload);
      return {
        ...state,
        current: {
          ...state.current,
          ...action.payload.data
        }
      };
    case FETCH_THEMES_CLASSE:
      return {
        ...state,
        current: {
          ...state.current,
          themes: action.payload.data
        }
      };
    default:
      return state;
  }
};
