import {
  FETCH_THEMES,
  FETCH_CURRENT_THEME,
  COMPLETE_ACTIVITY,
  UPDATE_THEME_TITLE,
  CREATE_ACTIVITY,
  DELETE_ACTIVITY
} from "../actions";

const initialState = {
  list: [],
  current: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_THEMES:
      return {
        ...state,
        list: action.payload.data
      };
    case FETCH_CURRENT_THEME:
      return {
        ...state,
        current: action.payload.data
      };
    case UPDATE_THEME_TITLE:
      return {
        ...state,
        current: {
          ...state.current,
          name: action.payload.title
        }
      };
    case COMPLETE_ACTIVITY:
      return {
        ...state,
        current: {
          ...state.current,
          activities: state.current.activities.map(
            item =>
              item.uuid === action.payload ? { ...item, done: true } : item
          )
        }
      };
    case CREATE_ACTIVITY:
      return {
        ...state,
        current: {
          ...state.current,
          activities: [...state.current.activities, action.payload.data]
        }
      };
    case DELETE_ACTIVITY:
      return {
        ...state,
        current: {
          ...state.current,
          activities: state.current.activities.filter(
            item => item.uuid !== action.payload
          )
        }
      };

    default:
      return state;
  }
};
