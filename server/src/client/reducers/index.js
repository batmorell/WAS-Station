import { combineReducers } from "redux";
import usersReducer from "./usersReducer";
import authReducer from "./authReducer";
import adminsReducer from "./adminsReducer";
import classesReducer from "./classesReducer";
import themesReducer from "./themesReducer";
import activityReducer from "./activityReducer";
import stationReducer from "./stationReducer";
import studentReducer from "./studentReducer";

export default combineReducers({
  users: usersReducer,
  auth: authReducer,
  admins: adminsReducer,
  classes: classesReducer,
  themes: themesReducer,
  activity: activityReducer,
  station: stationReducer,
  student: studentReducer
});
