import { FETCH_ACTIVITY, CHECK_SOLUCE, REMOVE_CURRENT_ACTIVITY, FETCH_EVAL_ACTIVITY_FOR_STUDENT } from "../actions";

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_ACTIVITY:
      return action.payload.data;
    case FETCH_EVAL_ACTIVITY_FOR_STUDENT:
      return action.payload.data;
    case CHECK_SOLUCE:
      return action.payload.data;
    case REMOVE_CURRENT_ACTIVITY:
      return {};
    default:
      return state;
  }
};
