import { FETCH_THEMES_FOR_STUDENT } from "../actions";

const initialState = {
  themeList: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_THEMES_FOR_STUDENT:
      return {
        ...state,
        themeList: action.payload.data
      };
    default:
      return state;
  }
};
