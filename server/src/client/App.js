import React from "react";
import { connect } from "react-redux";
import { renderRoutes } from "react-router-config";
import { Link } from "react-router-dom";
import { Layout } from "antd";
import requireAuth from "./components/hocs/requireAuth";
import { fetchCurrentUser } from "./actions";
import Sider from "./components/Sider";

const { Content, Footer } = Layout;

class App extends React.Component {
  render() {
    return (
      <Layout>
        <Sider role={this.props.auth.role} />
        <Layout style={{ marginLeft: 200 }}>
          <Content style={{ minHeight: "100vh" }}>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>{renderRoutes(this.props.route.routes)}</div>
          </Content>
          <Footer style={{ textAlign: "center" }}>WAS Station ©2017 Created by WAS</Footer>
        </Layout>
      </Layout>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default {
  component: connect(mapStateToProps, { fetchCurrentUser })(requireAuth(App))
};
