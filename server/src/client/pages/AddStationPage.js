import React, { Component } from "react";
import { Button, Row, Form, Icon, Input, Modal } from "antd";
import { connect } from "react-redux";
import { createStation } from "../actions";

const FormItem = Form.Item;

class AddStationPage extends Component {
  showModal = () => {
    Modal.info({
      title: `Votre code d'activation`,
      content: (
        <div>
          <p>Voici le code d&apos;activation de votre station</p>
          <h1 style={{ textAlign: "center" }}>{this.state.code}</h1>
        </div>
      ),
      onOk: () => {
        this.props.history.goBack();
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.createStation(values).then(response => {
          this.setState({ code: response.data.code });
          this.showModal();
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}>
            <Icon type="left" />Retour
          </Button>
        </Row>
        <Row>
          <h1 style={{ textAlign: "center" }}>Ajouter une nouvelle station</h1>
        </Row>
        <FormItem label="Nom">
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Vous devez renseigner le nom de la station" }]
          })(<Input placeholder="Nom de la Station" />)}
        </FormItem>
        <FormItem label="Établissement">
          {getFieldDecorator("place", {
            rules: [{ required: true, message: "Vous devez renseigner l'établissement" }]
          })(<Input placeholder="Établissement" />)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Créer
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedAddStationForm = Form.create()(AddStationPage);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { createStation })(WrappedAddStationForm)
};
