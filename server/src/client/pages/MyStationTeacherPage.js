import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { Button, Row, Col } from "antd";
import { connect } from "react-redux";
import { getMyStations } from "../actions";

class MyStationPage extends Component {
  componentDidMount() {
    this.props.getMyStations(this.props.auth.uuid);
  }
  head = () => (
    <Helmet>
      <title>WAS - Ma station</title>
      <meta property="og:title" content="WAS - Login" />
    </Helmet>
  );

  renderStationList = () =>
    this.props.station.list.map(element => (
      <Col
        key={element.uuid}
        md={{ span: 24 }}
        sm={{ span: 24 }}
        xs={{ span: 24 }}
        style={{ marginBottom: 20, padding: 20, backgroundColor: "#fafafa" }}
        onClick={() => this.navigateToStation(element.uuid)}
      >
        <Row key={element.uuid} type="flex" justify="space-between">
          <Col>
            <h2>{element.name}</h2>
          </Col>
          <Col type="flex" justify="space-between">
            <Button type="primary">Consulter</Button>
          </Col>
        </Row>
      </Col>
    ));

  render() {
    return (
      <div>
        <Row type="flex" justify="end">
          {this.props.auth.role === "teacher" && (
            <Col>
              <Link to="/clientStation/add">
                <Button type="primary">Ajouter une station</Button>
              </Link>
            </Col>
          )}
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <h1>Mes Stations</h1>
          </Col>
        </Row>
        <Row>{this.renderStationList()}</Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { auth: state.auth, station: state.station };
}

export default {
  component: connect(mapStateToProps, { getMyStations })(MyStationPage)
};
