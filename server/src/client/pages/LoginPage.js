import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import axios from "axios";
import { Redirect } from "react-router-dom";
import { fetchCurrentUser, authenticateUser } from "../actions";
import { Form, Icon, Input, Button, Layout, Row, Col } from "antd";

const { Header, Content, Footer } = Layout;
const FormItem = Form.Item;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      message: ""
    };
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      this.setState({ isLoading: true });
      if (!err) {
        this.props.authenticateUser(values).then(res => {
          this.setState({
            isLoading: false,
            message: res !== undefined && res.message
          });
        });
      }
    });
  };

  hasErrors = fieldsError => {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  };

  head() {
    return (
      <Helmet>
        <title>{`WAS - Login`}</title>
        <meta property="og:title" content="WAS - Login" />
      </Helmet>
    );
  }

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;

    const userNameError =
      isFieldTouched("username") && getFieldError("username");
    const passwordError =
      isFieldTouched("password") && getFieldError("password");
    return this.props.auth === false ? (
      <Layout>
        {this.head()}
        <Content>
          <Row
            type="flex"
            justify="center"
            align="middle"
            style={{ minHeight: "100vh" }}
          >
            <Col span={8}>
              <img src="img/logo-small.png" style={{ width: "100%" }} />
              {this.state.message.length != 0 && (
                <div className="info-login" align="middle">
                  {this.state.message}
                </div>
              )}
              <Form layout="horizontal" onSubmit={this.handleSubmit}>
                <FormItem
                  validateStatus={userNameError ? "error" : ""}
                  help={userNameError || ""}
                >
                  {getFieldDecorator("username", {
                    rules: [
                      {
                        required: true,
                        message: "Vous devez entrer votre username"
                      }
                    ]
                  })(
                    <Input
                      prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                      placeholder="Username"
                    />
                  )}
                </FormItem>
                <FormItem
                  validateStatus={passwordError ? "error" : ""}
                  help={passwordError || ""}
                >
                  {getFieldDecorator("password", {
                    rules: [
                      {
                        required: true,
                        message: "Il manque votre mot de passe :("
                      }
                    ]
                  })(
                    <Input
                      prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                      type="password"
                      placeholder="Password"
                    />
                  )}
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={
                      this.hasErrors(getFieldsError()) || this.state.isLoading
                    }
                  >
                    {this.state.isLoading ? (
                      <Icon type="loading" />
                    ) : (
                      "Connexion"
                    )}
                  </Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Content>
      </Layout>
    ) : (
      <Redirect to="/" />
    );
  }
}

const WrappedLogin = Form.create()(Login);

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default {
  component: connect(mapStateToProps, { fetchCurrentUser, authenticateUser })(
    WrappedLogin
  )
};
