import React, { Component } from "react";
import { Button, Row, Col, Icon, Divider } from "antd";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { fetchCurrentTheme, fetchActivity } from "../actions";

class ThemePage extends Component {
  constructor(props) {
    super(props);

    !props.themes.current && props.history.push("/themes");
  }

  navigateToActivity = id => {
    this.props.fetchActivity(id).then(() => this.props.history.push(`/activity/${id}`));
  };

  head() {
    return (
      <Helmet>
        <title>{this.props.themes.current.name}</title>
      </Helmet>
    );
  }

  renderActivitiesList = () =>
    this.props.themes.current.activities.map(item => (
      <Col
        key={item.uuid}
        md={{ span: 24 }}
        sm={{ span: 24 }}
        xs={{ span: 24 }}
        style={{ marginBottom: 20, padding: 20, backgroundColor: "#fafafa" }}
        onClick={() => this.navigateToActivity(item.uuid)}
      >
        <Row type="flex" justify="space-between">
          <Col>
            <h2>{item.name}</h2>
          </Col>
          <Col type="flex" justify="space-between">
            {item.type === "eval" && (
              <span style={{ marginRight: 10 }}>{this.props.themes.current.score.correct && `${this.props.themes.current.score.correct}/${this.props.themes.current.score.total}`}</span>
            )}
            {item.done ? (
              <Button style={{ backgroundColor: "rgb(120, 193, 82)" }} shape="circle">
                <Icon type="check" style={{ color: "white" }} />
              </Button>
            ) : (
              <Button disabled={item.done} type="primary">
                Commencer
              </Button>
            )}
          </Col>
        </Row>
      </Col>
    ));

  render() {
    const theme = this.props.themes.current;
    return (
      theme && (
        <div>
          {this.head()}
          <Row type="flex" justify="space-between">
            <Col>
              <Button type="primary" onClick={() => this.props.history.goBack()}>
                <Icon type="left" />Retour
              </Button>
            </Col>
          </Row>
          <h1 style={{ textAlign: "center" }}>
            {theme.name} {theme.score.correct && `- ${theme.score.correct}/${theme.score.total}`}
          </h1>
          <Divider>Liste des activités</Divider>
          <Row type="flex" justify="space-around">
            {this.renderActivitiesList()}
          </Row>
        </div>
      )
    );
  }
}

function mapStateToProps(state) {
  return state;
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchCurrentTheme, fetchActivity })(ThemePage)
};
