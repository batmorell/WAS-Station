import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { Button, Row, Col, Divider } from "antd";
import { connect } from "react-redux";
import { fetchDataStation } from "../actions";

class MyStationPage extends Component {
  componentDidMount() {
    this.props.fetchDataStation();
  }
  head = () => (
    <Helmet>
      <title>WAS - Ma station</title>
      <meta property="og:title" content="WAS - Login" />
    </Helmet>
  );

  renderDataStation = () =>
    this.props.station.dataStation.map(element => {
      const date = new Date(element.date);
      switch (element.type) {
        case "H":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Humidité </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data} %</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "U":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> UV </h3>
              </Row>

              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data}</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "T":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Température </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data} °C</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "V":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Vent </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data} km/h</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "L":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Pluie </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data}</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "O":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Poussière </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data}</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        case "P":
          return (
            <Col md={{ span: 7, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} style={{ margin: 20, backgroundColor: "#fafafa", paddingTop: 20, paddingBottom: 20 }}>
              <Row>
                <h3 style={{ textAlign: "center" }}> Pression </h3>
              </Row>
              <Row>
                <h1 style={{ textAlign: "center" }}>{element.data} hPa</h1>
              </Row>
              <Row>
                <p style={{ textAlign: "center" }}>{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}h${
                  date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
                }`}</p>
              </Row>
            </Col>
          );
        default:
          return null;
      }
    });

  render() {
    return (
      <div>
        <Row type="flex" justify="end">
          {this.props.auth.role === "teacher" && (
            <Col>
              <Link to="/clientStation/add">
                <Button type="primary">Ajouter une station</Button>
              </Link>
            </Col>
          )}
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <h1>Ma Station</h1>
          </Col>
        </Row>
        <Row>{this.renderDataStation()}</Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { auth: state.auth, station: state.station };
}

export default {
  component: connect(mapStateToProps, { fetchDataStation })(MyStationPage)
};
