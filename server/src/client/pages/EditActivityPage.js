import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Form, Icon, Input, Button, Row } from "antd";

const FormItem = Form.Item;
const { TextArea } = Input;

class EditActivityForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .put(`/api/activity/${this.props.activity.uuid}`, {
            type: this.props.activity.type,
            data: `${values.data}`
          })
          .then(() => {
            this.props.history.goBack();
          })
          .catch(() => console.log(err));
      }
    });
  };

  render() {
    console.log(this.props.activity);
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}>
            <Icon type="left" />Retour
          </Button>
          <h1>Update de l'activité {this.props.activity.name}</h1>
        </Row>
        <FormItem label="Données de l'activité">
          {getFieldDecorator("data", {
            rules: [
              {
                required: true,
                message: "Vous devez renseigner les données sur l'activité"
              }
            ]
          })(<TextArea placeholder="JSON de l'activité" autosize />)}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Créer
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedEditActivityForm = Form.create()(EditActivityForm);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps)(WrappedEditActivityForm)
};
