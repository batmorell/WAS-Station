import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Form, Icon, Input, Button, Row, Select } from "antd";
import { createActivity } from "../actions";

const FormItem = Form.Item;
const { Option } = Select;
const { TextArea } = Input;

class AddActivityForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var invalidJSON = false;

        try {
          JSON.parse(values.data);
        } catch (error) {
          invalidJSON = true;
        }

        if (!invalidJSON) {
          this.props
            .createActivity({
              name: values.name,
              type: values.type,
              idTheme: this.props.themes.current.uuid
            })
            .then(response =>
              axios
                .put(`/api/activity/${response.data.uuid}`, {
                  type: response.data.type,
                  data: `${values.data}`
                })
                .then(() => {
                  this.props.history.goBack();
                })
                .catch(() => console.log(err))
            );
        } else {
          this.setState({
            message: "Le JSON n'est pas valide"
          });
        }
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}>
            <Icon type="left" />Retour
          </Button>
          <h1>Créer une nouvelle activité</h1>
        </Row>
        <FormItem label="Nom">
          {getFieldDecorator("name", {
            rules: [
              {
                required: true,
                message: "Vous devez renseigner le nom de l'activité"
              }
            ]
          })(<Input placeholder="Nom de l'activité" />)}
        </FormItem>
        <FormItem label="Type de l'activité">
          {getFieldDecorator("type", {
            rules: [
              {
                required: true,
                message: "Vous devez renseigner le type de l'activité"
              }
            ]
          })(
            <Select placeholder="Type de l'activité">
              <Option value="eval">Évaluation</Option>
              <Option value="quizz">Quizz</Option>
              <Option value="dragDrop">Drag & Drop</Option>
              <Option value="trucARelier">Truc à relier</Option>
              <Option value="instruction">Instruction</Option>
            </Select>
          )}
        </FormItem>
        {this.state.message.length != 0 && (
          <div className="info-login" align="middle">
            {this.state.message}
          </div>
        )}
        <FormItem label="Données de l'activité">
          {getFieldDecorator("data", {
            rules: [
              {
                required: true,
                message: "Vous devez renseigner les données sur l'activité"
              }
            ]
          })(<TextArea placeholder="JSON de l'activité" autosize />)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Créer
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedAddActivityForm = Form.create()(AddActivityForm);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { createActivity })(WrappedAddActivityForm)
};
