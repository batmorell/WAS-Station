import React, { Component } from "react";
import { Row, Col, Button, Icon } from "antd";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import ThemeCardTeacher from "../components/ThemeCardTeacher";
import { fetchThemesForStudent, fetchEvalActivityForStudent } from "../actions";

class ThemeList extends Component {
  componentDidMount() {
    this.props.fetchThemesForStudent(this.props.match.params.id);
  }

  navigate = themeId => {
    this.props.fetchEvalActivityForStudent(themeId, this.props.match.params.id).then(() => this.props.history.push(`/activity/12`));
  };

  head = () => (
    <Helmet>
      <title>Info élève</title>
    </Helmet>
  );

  renderThemeList = () =>
    this.props.student.themeList.map(item => (
      <Col key={item.uuid} md={{ span: 8 }} sm={{ span: 24 }} xs={{ span: 24 }}>
        <ThemeCardTeacher teacher={false} theme={item} navigate={this.navigate} />
      </Col>
    ));

  render() {
    return (
      <div>
        {this.head()}
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}>
            <Icon type="left" />Retour
          </Button>
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <h1 style={{ textAlign: "center" }}>Progression élève</h1>
          </Col>
        </Row>
        <Row gutter={20}>{this.renderThemeList()}</Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchThemesForStudent, fetchEvalActivityForStudent })(ThemeList)
};
