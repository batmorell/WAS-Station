import React, { Component } from "react";
import { Redirect } from "react-router";
import axios from "axios";
import { Link } from "react-router-dom";
import CardLineChart from "../components/CardLineChart";
import UserInfoCard from "../components/UserInfoCard";
import { connect } from "react-redux";
import { Form, Icon, Input, Button, Checkbox, Row, Col } from "antd";

const FormItem = Form.Item;

class AddClasseForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .post("/api/users/createStudent", {
            ...values,
            classeUuid: this.props.classes.current.uuid
          })
          .then(result => {
            this.props.history.goBack();
          })
          .catch(err => console.log(err));
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        {this.props.classes.current ? (
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Row type="flex" justify="space-between">
              <Button type="primary" onClick={() => this.props.history.goBack()}>
                <Icon type="left" />Retour
              </Button>
            </Row>
            <Row>
              <h1 style={{ textAlign: "center" }}>Ajouter un nouvel élève en {this.props.classes.current.name}</h1>
            </Row>
            <FormItem label="Nom">
              {getFieldDecorator("username", {
                rules: [{ required: true, message: "Vous devez renseigner le nom de l'élève" }]
              })(<Input placeholder="Nom de l'éleve" />)}
            </FormItem>
            <FormItem label="Mot de passe">
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "Vous devez renseigner un mot de passe" }]
              })(<Input placeholder="Mot de passe" />)}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Créer
              </Button>
            </FormItem>
          </Form>
        ) : (
          <h1>Ajouter un élève</h1>
        )}
      </div>
    );
  }
}

const WrappedAddClasseForm = Form.create()(AddClasseForm);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps)(WrappedAddClasseForm)
};
