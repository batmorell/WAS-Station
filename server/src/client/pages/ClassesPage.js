import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { Row, Table, Button } from "antd";
import { fetchMyClasses } from "../actions";

const columns = [
  {
    title: "Nom",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Action",
    dataIndex: "uuid",
    key: "uuid",
    render: uuid => (
      <Link to={`/classe/${uuid}`}>
        <Button type="primary">Consulter</Button>
      </Link>
    )
  }
];

class Classes extends Component {
  componentDidMount() {
    this.props.fetchMyClasses();
  }

  head = () => (
    <Helmet>
      <title>Liste de mes classes</title>
    </Helmet>
  );

  render() {
    return (
      <div className="center-align">
        {this.head()}
        <Row type="flex" justify="space-between">
          <h1>Mes classes</h1>
          <Link to="/classes/add">
            <Button type="primary">Ajouter une classe</Button>
          </Link>
        </Row>
        <Table dataSource={this.props.classes.list} columns={columns} rowKey={classe => classe.uuid} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { fetchMyClasses })(Classes)
};
