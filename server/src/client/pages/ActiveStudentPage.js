import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Card, Tree, Icon } from "antd";
import { fetchActiveStudent, closeConnection } from "../sockets";

const { TreeNode } = Tree;

class ActiveStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStudents: []
    };
    fetchActiveStudent(props.auth.uuid, (err, results) => {
      this.setState({
        activeStudents: results
      });
    });
  }
  componentWillUnmount = () => {
    closeConnection();
  };
  getColor = (elementId, currentId, isDone) => {
    if (elementId === currentId) return "rgb(37, 147, 252)";
    if (isDone) return "rgb(120, 193, 82)";
    return null;
  };
  renderStudentList = () => this.state.activeStudents.map(item => <h1>{item.person.name}</h1>);
  render() {
    return (
      <List
        grid={{ gutter: 16, column: 4 }}
        dataSource={this.state.activeStudents}
        header={<h1 style={{ textAlign: "center" }}>Liste des étudiants actifs</h1>}
        locale={{ emptyText: "Aucun étudiant actif" }}
        renderItem={item => (
          <List.Item>
            <Card title={item.person.name}>
              <Tree defaultExpandedKeys={["0-0"]}>
                <TreeNode title={item.theme.name} key="0-0">
                  {item.listOfActivities.map((element, index) => (
                    <TreeNode
                      title={
                        <span style={{ color: this.getColor(element.uuid, item.current, item.isDone[index]) }}>
                          {item.isDone[index] && !(element.uuid === item.current) && <Icon type="check" />}
                          {element.name}
                        </span>
                      }
                      key={`0-0-${element.uuid}`}
                    />
                  ))}
                </TreeNode>
              </Tree>
            </Card>
          </List.Item>
        )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps)(ActiveStudent)
};
