import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Button, Row, Col, Icon, Tag, Divider, Select } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchActivity } from "../actions";
import { Redirect } from "react-router-dom";

const Option = Select.Option;

class EditThemePage extends Component {
  constructor(props) {
    super(props);

    !props.themes.current && props.history.push("/themes");
  }

  navigateToActivity = id => {
    this.props.fetchActivity(id).then(() => this.props.history.push(`/activity/${id}`));
  };

  renderActivitiesList = () => this.props.themes.current.activities.map(item => (
        <Col
          key={item.uuid}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
          style={{ marginBottom: 20, padding: 20, backgroundColor: "#fafafa" }}
          onClick={() => this.navigateToActivity(item.uuid)}
        >
          <Row type="flex" justify="space-between">
            <Col>
              <h2>{item.name}</h2>
            </Col>
            <Col>
              {item.done ? (
                <Button style={{ backgroundColor: "rgb(120, 193, 82)" }} shape="circle">
                  <Icon type="check" style={{ color: "white" }} />
                </Button>
              ) : (
                <Button disabled={item.done} type="primary">
                  {this.props.auth.role === "teacher" ? "Consulter" : "Commencer"}
                </Button>
              )}
            </Col>
          </Row>
        </Col>
      ));

  renderClassesList = () => (
      <Select
        mode="multiple"
        style={{ width: "100%" }}
        placeholder="Sélectionner les classes pouvant accéder à cette activité"
        defaultValue={this.props.themes.current.listOfClasses.map(item => item.uuid)}
        onChange={this.handleChange}
      >
        {this.props.classes.list.map(item => <Option key={item.uuid}>{item.name}</Option>)}
      </Select>
    );

  render() {
    return (
      this.props.themes.current && (
        <div>
          <Row type="flex" justify="space-between">
            <Col>
              <Button type="primary" onClick={() => this.props.history.goBack()}>
                <Icon type="left" />Retour
              </Button>
            </Col>
          </Row>
          <h1 style={{ textAlign: "center" }}>Édition du thème {this.props.themes.current.name}</h1>
          <Divider>Liste des classes</Divider>
          <Row>{this.renderClassesList()}</Row>
          <Divider>Liste des activités</Divider>
          <Row type="flex" justify="space-around">
            {this.renderActivitiesList()}
          </Row>
        </div>
      )
    );
  }
}

function mapStateToProps(state) {
  return state;
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchActivity })(EditThemePage)
};
