import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import { Helmet } from "react-helmet";
import CardLineChart from "../components/CardLineChart";
import UserInfoCard from "../components/UserInfoCard";
import StationInfoCard from "../components/StationInfoCard";
import { fetchLastDataStation, fetchDataStation } from "../actions";

class Home extends Component {
  componentDidMount() {
    this.props.fetchLastDataStation();
    this.props.fetchDataStation();
  }

  head = () => (
    <Helmet>
      <title>Home</title>
    </Helmet>
  );

  renderChart = () =>
    this.props.station.lastDataStation.map(element => (
      <Col md={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
        <CardLineChart type={element.type} id={element.type} data={element.data} />
      </Col>
    ));

  render() {
    const { dataStation } = this.props.station;
    console.log(dataStation.filter(item => item.type === "T"));
    return (
      <div className="center-align">
        {this.head()}
        <Row type="flex" justify="space-around" gutter={20}>
          <Col md={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <StationInfoCard data={dataStation} pos={1} />
          </Col>
          <Col md={{ span: 8, order: 2 }} sm={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
            <UserInfoCard name={this.props.auth.name} role={this.props.auth.role} image="img/avatar.png" />
          </Col>
          <Col md={{ span: 8, order: 3 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <StationInfoCard data={dataStation} pos={2} />
          </Col>
        </Row>
        <Row type="flex" justify="space-around" gutter={20}>
          {this.renderChart()}
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { fetchLastDataStation, fetchDataStation })(Home)
};
