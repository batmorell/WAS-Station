import React, { Component } from "react";
import { Table, Row, Button, Icon, Progress, Tabs } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { fetchAClasse, fetchThemesClasse } from "../actions";

const TabPane = Tabs.TabPane;

const columns = [
  {
    title: "Nom",
    dataIndex: "name",
    key: "name",
    render: (text, record) => <Link to={`/student/${record.uuid}`}>{text}</Link>
  },
  {
    title: "Note",
    dataIndex: "uuid",
    key: "uuid",
    render: (text, record) => <p>{record.best === 0 ? `Non évalué` : `${record.eval}/${record.best}`}</p>
  },
  {
    title: "Progression",
    dataIndex: "uuid",
    key: "uuid",
    render: (text, record) => <Progress percent={parseInt(record.number / record.total * 100, 10)} />
  }
];

const columnsTheme = [
  {
    title: "Nom",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Progression",
    dataIndex: "uuid",
    key: "uuid",
    render: (text, record) => <Progress percent={parseInt(record.number / record.total * 100, 10)} />
  }
];

class Classe extends Component {
  componentDidMount() {
    this.props.fetchAClasse(this.props.match.params.id);
    this.props.fetchThemesClasse(this.props.match.params.id);
  }

  head = () => <Helmet>{this.props.classes.current ? <title>{`Ma classe de ${this.props.classes.current.name}`}</title> : <title>Ma classe</title>}</Helmet>;

  render() {
    return (
      <div>
        {this.head()}
        {this.props.classes.current ? (
          <div>
            <Row type="flex" justify="space-between">
              <Button type="primary" onClick={() => this.props.history.goBack()}>
                <Icon type="left" />Retour
              </Button>
            </Row>
            <h1 style={{ textAlign: "center" }}>Ma classe de {this.props.classes.current.name}</h1>
            <Row>
              <Tabs defaultActiveKey="2">
                <TabPane tab="Élèves" key="2">
                  <Row style={{ marginTop: 40 }} type="flex" justify="space-between">
                    <h1>Liste des élèves</h1>
                    <Link to="/classes/addStudent">
                      <Button type="primary">Ajouter un élève</Button>
                    </Link>
                  </Row>
                  <Table dataSource={this.props.classes.current.students} columns={columns} size="middle" />
                </TabPane>
                <TabPane tab="Thème" key="1">
                  <h1 style={{ marginTop: 40 }}>Liste des thèmes</h1>
                  <Table dataSource={this.props.classes.current.themes} columns={columnsTheme} pagination={false} size="middle" />
                </TabPane>
              </Tabs>
            </Row>
          </div>
        ) : (
          <div className="center-align">
            <h1>Ma classe</h1>
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { fetchAClasse, fetchThemesClasse })(Classe)
};
